function hidePartOfSection(arr) {
    if (arr.length >= 10) {
        for (let item = 10; item < arr.length; item++) {
            arr[item].style.display = 'none'
        }
    } else {
        arr.forEach(item => item.style.display = 'block')
    }
}

const more = document.body.querySelector('.showMore')

const filterTabs = document.body.querySelector(".tabs-nav")
filterTabs.addEventListener("click", (event) => {
    const tabImages = [...document.querySelectorAll(".cards__item")]
    more.style.display = 'none'
    for (let i = 0; i < tabImages.length; i++) {
        if (event.target !== filterTabs && event.target.getAttribute("data-type")
            !== tabImages[i].getAttribute("data-img")) {
            tabImages[i].style.display = "none"
        } else if (event.target !== filterTabs) {
            tabImages[i].style.display = "block"
        }
        if (event.target.getAttribute('data-type') === "all") {
            for (let i = 0; i < tabImages.length; i++) {
                more.style.display = 'block'
                tabImages[i].style.display = "block"
                hidePartOfSection(tabImages)
            }
        }
    }
})
const cardList = [...document.querySelectorAll(".cards__item")]
hidePartOfSection(cardList)

filterTabs.addEventListener("click", (event) => {
    const filters = [...document.querySelectorAll('.tabs-nav__item')]
    if (event.target !== filterTabs) {
        filters.forEach(elem => {
            elem.classList.remove('active')
        })
        filters.find((elem) => {
            if (event.target.closest("li") === elem) {
                if (!event.target.classList.contains('active')) {
                    event.target.closest("li").classList.toggle('active')
                }
            }
        })
    }
    console.log(event.target)

})

more.addEventListener('click', () => {
    more.classList.toggle('show')
    if (more.classList.contains("show")) {
        more.textContent = 'Приховати'
        for (let i = 0; i < cardList.length; i++) {
            cardList[i].style.display = "block"
        }
    } else {
        more.textContent = 'Показати більше'
        for (let i = 10; i < cardList.length; i++) {
            cardList[i].style.display = "none"
        }
    }
})
