$('a').on('click', function() {
    let href = $(this).attr('href');
    $('html, body').animate({
        scrollTop: $(href).offset().top
    }, {
        duration: 600,
        easing: "linear"
    });
    return false;
})

window.addEventListener('scroll', ()=>{
    let scroller = window.pageYOffset;
    let screen = document.documentElement.clientHeight;
    if (scroller > screen){
        $('.back-to-top').css("display", "block")
    }
    if (scroller < screen){
        $('.back-to-top').css("display", "none")
    }
});
