let http = require("http");
let url = require('url');
let fs = require("fs");
const fetch = require('node-fetch')

const api = {
    token:'1929367326:AAGPLxVUZ8YljVs4MvUFDtzn8LzmZV34K4Q',
    chatId:'-554157437'
}

http.createServer(function(request, response) {
    console.log(`Запрошенный адрес: ${request.url}`);
    let urlRequest = url.parse(request.url, true);
   if (request.method == "POST") {
        let body = "";
        request.on("data", chunk => {
            body +=  chunk.toString();
        });
        request.on("end", () => {
            let userinfo = JSON.parse(body);
            console.log(userinfo)
            fetch(`https://api.telegram.org/bot${api.token}/sendMessage?chat_id=${api.token}&parse_mode=html&text=${userinfo}`, {
                method: 'post',
                body:    JSON.stringify(userinfo),
                headers: { 'Content-Type': 'application/json' },
            })
                .then(res => res.json())
                .then(res => console.log(res));
        });
    }
    else{
        let filePath = "index.html";
        if(request.url !== "/"){
            filePath = request.url.substr(1);
        }
        fs.readFile(filePath, function(error, data){
            if(error){
                response.statusCode = 404;
                response.end("Resource not found!");
            }
            else{
                if (filePath.slice(-3) == "css") {
                    response.setHeader('Content-Type', 'text/css')
                }
                if (filePath.slice(-2) == "js") {
                    response.setHeader('Content-Type', 'text/js')
                }
                response.end(data);
            }
        });
    }
}).listen(3000);